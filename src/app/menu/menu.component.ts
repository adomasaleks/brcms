import { Component, HostBinding, Input, OnInit } from '@angular/core';
import {Component as BrComponent, Menu, Page, isMenu} from '@bloomreach/spa-sdk';

@Component({
  selector: 'ul.br-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent  {
@Input() component!:BrComponent;
@Input() page!:Page;

@HostBinding('class.has-edit-button')

get isPreview(){
  return this.page.isPreview();
}

get menu(){
  const menuRef = this.component.getModels<MenuModels>()?.menu;
  const menu = menuRef && this.page.getContent<Menu>(menuRef);

  return isMenu(menu) ? menu:undefined;
}
  constructor() { }

  ngOnInit(): void {
  }

}
