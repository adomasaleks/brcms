import { Component, Input } from '@angular/core';
import { Component as BrComponent } from '@bloomreach/spa-sdk';

@Component({
  selector: 'app-banner',
  template: 'Banner: {{ component.getName() }}',
})
export class BannerComponent {
  @Input() component!: BrComponent;
}

