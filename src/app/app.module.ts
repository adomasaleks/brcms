import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrSdkModule} from '@bloomreach/ng-sdk';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BannerComponent} from './banner/banner.component';
import { InlineContainerComponent } from './inline-container/inline-container.component';
import { MenuComponent } from './menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    InlineContainerComponent,
    MenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrSdkModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
