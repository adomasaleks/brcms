import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { BrPageComponent } from '@bloomreach/ng-sdk';

import { BannerComponent } from './banner/banner.component';

@Component({
  selector: 'app-root',
  template: `
    <br-page [configuration]="configuration" [mapping]="mapping">
      <ng-template let-page="page">
        <header>
          <a [href]="page.getUrl('/')">Home</a>
          <ng-container brComponent="menu"></ng-container>
        </header>
        <section>
          <ng-container brComponent="main"></ng-container>
        </section>
        <footer>
          <ng-container brComponent="footer"></ng-container>
        </footer>
      </ng-template>
    </br-page>
  `,
})
export class AppComponent {
  configuration: BrPageComponent['configuration'];

  mapping = {
    Banner: BannerComponent,
  };

  constructor(location: Location) {
    this.configuration = {
      cmsBaseUrl: 'http://localhost:8080/site',
      request: { path: location.path() },
    };
  }
}