import { Component, Input } from '@angular/core';
import { Component as BrComponent } from '@bloomreach/spa-sdk';

@Component({
  selector: 'app-inline-container',
  template: `
    <div>
      <span *ngFor="let child of component.getChildren()">
        <ng-container [brComponent]="child"></ng-container>
      </span>
    </div>
  `,
})
export class InlineContainerComponent {
  @Input() component!: BrComponent;
}