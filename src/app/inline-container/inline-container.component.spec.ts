import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineContainerComponent } from './inline-container.component';

describe('InlineContainerComponent', () => {
  let component: InlineContainerComponent;
  let fixture: ComponentFixture<InlineContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InlineContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
